@extends('layouts.app') @section('content')
<div class="col-lg-10 col-lg-offset-1">
    <h1 class="pull-right">اﻷقسام </h1><br><a href="{{ url('division/create') }}" class="btn btn-primary btn-sm">إضافة قسم</a>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th class="text-center">Actions</th>
                    <th class="text-center">اﻹختصار</th>
                    <th class="text-center">اﻹسم</th>
                </tr>
            </thead>
            <tbody>
                @foreach($divisions as $item)
                <tr class="text-center">
                <td>
                        <a href="{{ url('division/' . $item->id) }}">
                                <button type="submit" class="btn btn-success btn-xs">عرض</button>
                            </a>
                        <a href="{{ url('division/' . $item->id . '/edit') }}">
                                <button type="submit" class="btn btn-primary btn-xs">تبديل</button>
                        {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['division', $item->id],
                        'style' => 'display:inline'
                        ]) !!}
                        {!! Form::submit('مسح', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                    <td> {{$item->abbr}} </td>
                    <td><a href="{{url('division/'.$item->id)}}"> {{ $item->libelle }}</a></td>
                </tr>
            @endforeach
    </tbody>
</table>
{{$divisions->links() }} 
</div>
</div> <!-- col-lg-10 col-lg-offset-1 -->
@endsection