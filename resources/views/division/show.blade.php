@extends('layouts.app')

@section('content')
<div class="col-lg-8 col-lg-offset-2">
    <h1>Division</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Libelle</th><th>Abbréviation</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $division->libelle}}</td>
                    <td>{{ $division->abbr }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    </div><!-- col-lg-10 col-lg-offset-1 -->
    <div class="row">
    <div class="col col-md-8 col-md-offset-2">
    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
    </div>
    
@endsection
@section('js') 
@parent
<script src="{{URL::asset('js/highcharts.js')}}"></script>
<script src="{{URL::asset('js/exporting.js')}}"></script>
<script type="text/javascript">
Highcharts.chart('container', {
    chart: {
        type: 'line'
    },
    title: {
        text: 'عدد الزيارات الشهرية'
    },
    subtitle: {
        text: 'عمالة اشتوكة أيت باها'
    },
    xAxis: {
        categories: ['يناير', 'فبراير', 'مارس', 'أبريل', 'ماي', 'يونيو', 'يوليوز', 'غشت', 'شتنبر', 'أكتوبر', 'نونبر', 'دجنبر']
    },
    yAxis: {
        title: {
            text: 'عدد الزيارات'
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: [ {!! json_encode($point) !!} ]
});
</script>
@endsection