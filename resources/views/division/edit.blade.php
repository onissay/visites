@extends('layouts.app')

@section('content')
<div class="col-lg-8 col-xs-8 col-md-8 col-lg-offset-2">
    <h1>Modification Division</h1>
    <hr/>

    {!! Form::model($division, [
        'method' => 'PATCH',
        'url' => ['division', $division->id],
        'class' => 'form-horizontal'
    ]) !!}


    <table class="table">
       <fieldset>
     <tr>
        <td>
            Libelle:
        </td>
        <td class="row">
            <div class="col-xs-7">
               <input name="libelle" type="text" class="form-control" value="{{$division->libelle}}">
           </div>
       </td>
   </tr>

   <tr>
    <td>
        Abbréviation:
    </td>
    <td class="row">
        <div class="col-xs-7">
           <input type="text" name ="abbr"  class="form-control" name="" value="{{$division->abbr}}" >
       </div>
   </td>
</tr>


<tr>
    <td>
        <div class="form-group">
            <div class="col-sm-4">
                {!! Form::submit('Modifier', ['class' => 'btn btn-success  form-control']) !!}
            </div>
        </div>
    </td>
    <td></td>
</tr>
</div>
</fieldset>
{!! Form::close() !!}
</table>
@if ($errors->any())
<ul class="alert alert-danger">
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
@endif
</div>
</div><!-- col-lg-8 -->
@endsection
@section('css')
@parent
<link rel="stylesheet" href="{{URL::asset('js/jquery-ui/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('js/chosen/chosen.min.css')}}">
@endsection
@section('js')
@parent
<script src="{{URL::asset('js/jquery-ui/jquery-ui.min.js')}}" division="text/javascript"></script>
<script src="{{URL::asset('js/chosen/chosen.jquery.min.js')}}" division="text/javascript"></script>
<script>
  $(document).ready(function(){
    $("select").chosen({
      disable_search_threshold: 10,
      no_results_text: "N'existe pas",
      width: "100%"
  });
});
</script>
@endsection