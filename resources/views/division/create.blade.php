@extends('layouts.app') @section('content')
<div class="col-lg-10 col-lg-offset-1">
    <h1 class="pull-right">إضافة جديدة</h1>


    {!! Form::open(['url' => 'division', 'class' => 'form-horizontal']) !!}
    <table class="table ">
        <fieldset>
            <tr>
            <td class="row">
                    <div class="col-xs-7">
                        <input name="libelle" dir="rtl" type="text" class="form-control">
                    </div>
                </td>
                <td>
                    اﻹسم
                </td>
                
            </tr>

            <tr>
            <td class="row">
                    <div class="col-xs-7">
                        <input type="text" dir="rtl" name="abbr" class="form-control">
                    </div>
                </td>
                <td>
                    اﻹختصار
                </td>
            </tr>
            <tr>
            <td class="row">
                    <div class="col-xs-7 pull-right">
                        <input  type="checkbox" name="isControlable" id="" value="1">
                    </div>
                </td>
                <td>
                   مراقب
                </td>
       
            </tr>

            <tr>
                <td>
                    <div class="form-group">
                        <div class="col-sm-4">
                            {!! Form::submit('إضافة', ['class' => 'btn btn-success form-control']) !!}
                        </div>
                    </div>
                </td>
                <td></td>
            </tr>
        </fieldset>
        {!! Form::close() !!}
    </table>
    @if ($errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>https://stackoverflow.com/questions/23430205/laravel-notfoundhttpexception
        @endforeach
    </ul>
    @endif

</div>
@endsection @section('css') @parent
<link rel="stylesheet" href="{{URL::asset('js/jquery-ui/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('js/chosen/chosen.min.css')}}"> @endsection @section('js') @parent
<script src="{{URL::asset('js/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('js/chosen/chosen.jquery.min.js')}}" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $("select").chosen({
            disable_search_threshold: 10,
            no_results_text: "N'existe pas",
            width: "100%"
        });
    });
</script>
</div>
<!-- col-lg-10 col-lg-offset-1 -->
@endsection