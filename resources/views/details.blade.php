@extends("layouts.app") 
@section('content')

<div class="row">
	
	<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 col-xs-offset-2 col-sm-offset-2 col-lg-offset-2">
    <h3 class="row-fluid header smaller lighter purple pull-right">{{$visiteur->id}}  ﻻئحة زيارات {{$visiteur->getFullName()}} ر.ب.و</h3>
		<div class="timeline-container">
			<div class="timeline-label">
				<span class="label label-primary arrowed-in-right label-lg">
					<b>الزيارات</b>
				</span>
			</div>
            @foreach($visites as $visite) 
			<div class="timeline-items">
				<div class="timeline-item clearfix">
					<div class="timeline-info">
						<span class="label label-info label-sm">{{$visite->created_at->format('h:i')}}</span>
					</div>

					<div class="widget-box transparent">
						<div class="widget-header widget-header-small">
							<h5 class="smaller">
								<a href="#" class="blue">{{\App\Division::find($visite->division_id)->libelle}}</a>
							</h5>

							<span class="widget-toolbar no-border">
								<i class="icon-time bigger-110"></i>
								{{$visite->created_at}}
							</span>
						</div>

					</div>
				</div>
        @endforeach
			</div><!-- /.timeline-items -->
		</div>
	</div><!-- col-lg -->
</div><!-- row -->
@endsection