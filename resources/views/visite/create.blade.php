@extends('layouts.app')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="col-lg-10 col-lg-offset-1">
<br>
<table class="table table-condensed table-responsive">
  @if (count($errors) > 0)
  <div class="alert bg-danger" role="alert">
    @foreach ($errors->all() as $error)
  <svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"/></svg>
    {{ $error }} <br>
    @endforeach
  </div>
  @endif
  <div id="error"></div>
  {!! Form::open(['url' => 'visite', 'class' => 'form-group form-control']) !!}
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <tr class="page-header" style="background:azure">
    <td></td>
   <td class=" pull-right"><h1> زيارة جديدة</h1>
   </td>
 </tr>
<tr>
 <td class="row">
        <div class="col-xs-7">
         <input type="text" class="form-control" dir="rtl" name="visiteur_id" id="visiteur_id" required>
       </div>
  </td>
  <td>
   رقم البطاقة الوطنية:
 </td>

</tr>
<tr>
    <td class="row">
        <div class="col-xs-7">
          <input dir="rtl" class="form-group form-control" type="text" name="prenom" id="prenom" required>
        </div>
      </td>
  <td>  الاسم الشخصي:     </td>
</tr>
<tr>
    <td class="row">
        <div class="col-xs-7">
          <input dir="rtl" class="form-group form-control" type="text" name="nom" id="nom" required>
        </div>
      </td>
  <td>الاسم العائلي:</td>
</tr>
<tr>
    <td class="row">
        <div class="col-xs-7">
          <input dir="rtl" class="form-group form-control" type="text" name="adresse" id="adresse" >
        </div>
      </td>
  <td>العنوان</td>
</tr>
<tr>
    <td class="row">
        <div class="col-xs-7">
          <input class="form-group form-control" type="text" name="date_naissance" id="date_naissance" >
        </div>
      </td>
  <td>تاريخ الازدياد</td>
</tr>
<tr>
    <td class="row">
        <div class="col-xs-7">
          <input class="form-group form-control" type="text" name="tel" id="tel" >
        </div>
      </td>
  <td>الهاتف</td>
</tr>

<tr>
    <td>
        <div class="col-xs-7">
          {!! Form::select('division_id',$divisions, null, ['required'=>'required']) !!}
        </div>
      </td>
  <td>
    <label class="block clearfix">
      {!! Form::label('division_id','القسم - المصلحة') !!}
    </label>
  </td>
</tr>
 <tr> <td>
  <input type="submit" name="valider" class="btn btn-success" value="تأكبد">
  <input type="reset" name="reset" class="btn btn-info" value="مسح">
</td></tr>
{!! Form::close() !!}
</table>
</div> <!-- col-lg-10 col-lg-offset-1 -->
@endsection
<!-- section du code css -->
@section('css')
@parent
<link rel="stylesheet" href="{{URL::asset('js/jquery-ui/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('js/chosen/chosen.min.css')}}">
@endsection

<!-- section du code js -->
@section('js')
@parent
<script src="{{URL::asset('js/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('js/chosen/chosen.jquery.min.js')}}" type="text/javascript"></script>

<script>
  $(document).ready(function(){
 $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $("select").chosen({
      disable_search_threshold: 10,
      no_results_text: "N'existe pas",
      width: "95%",
      placeholder_text:"القسم - المصلحة",
      rtl:true
    });
    $.datepicker.regional['fr'] = {clearText: 'Effacer', clearStatus: '',
    closeText: 'Fermer', closeStatus: 'Fermer sans modifier',
    prevText: '&lt;Préc', prevStatus: 'Voir le mois précédent',
    nextText: 'Suiv&gt;', nextStatus: 'Voir le mois suivant',
    currentText: 'Courant', currentStatus: 'Voir le mois courant',
    monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
    'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
    monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
    'Jul','Aoû','Sep','Oct','Nov','Déc'],
    monthStatus: 'Voir un autre mois', yearStatus: 'Voir un autre année',
    weekHeader: 'Sm', weekStatus: '',
    dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
    dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
    dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
    dayStatus: 'Utiliser DD comme premier jour de la semaine', dateStatus: 'Choisir le DD, MM d',
    dateFormat: 'dd/mm/yy', firstDay: 0, 
    initStatus: 'Choisir la date', isRTL: false};
    $.datepicker.setDefaults($.datepicker.regional['fr']);
    $( "#date_naissance" ).datepicker({
      dateFormat: "d/m/yy",
   
    });
//id client vérification
$('#visiteur_id').on('input', function(event) {
 $.ajax({
  method:'POST',
  url: '{{url("/checkVisiteur")}}',
  data:{'id':$(this).val()},
  beforeSend: function( xhr ) {
    xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
  }
})
 .done(function( data ) {
  if(data){
    data = $.parseJSON(data);
    $('input[name="nom"]').attr({
      value: data.nom,
      disabled: 'disabled'
    });
    $('input[name="prenom"]').attr({
      value: data.prenom,
      disabled: 'disabled'
    });
    $('input[name="date_naissance"]').attr({
      value: data.date_naissance,
      disabled: 'disabled'
    });
    $('input[name="tel"]').attr({
      value: data.tel,
      disabled: 'disabled'
    });
    $('input[name="adresse"]').attr({
      value: data.adresse,
      disabled: 'disabled'
    });
    if(data.isBlocked==1){
      $('#error').html(data.error);
    }
  }else{
  $('#error').html(' ');
  $('input[name="nom"]').attr({ value: ''}).removeAttr('disabled');
  $('input[name="prenom"]').attr({value: ''}).removeAttr('disabled');
  $('input[name="adresse"]').attr({value: ''}).removeAttr('disabled');
  $('input[name="date_naissance"]').attr({value: ''}).removeAttr('disabled');
  $('input[name="tel"]').attr({value: ''}).removeAttr('disabled');
 }
});
});
});
</script>
@endsection