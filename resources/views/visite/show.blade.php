@extends('layouts.master')

@section('content')
<div id="page" class="réservations"></div>
<h1>Reservation</h1>
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">

        <thead>
            <tr>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>N° réservation</th>
                <td> {{$reservation->id}} </td>

            </tr>
            <tr>
                <th>N° de chambre</th>
                <td> {{$reservation->chambre_id}} </td>

            </tr>
            <tr>
                <th>ID client</th>
                <td> {{$reservation->client_id}} </td>

            </tr>
            <tr>
                <th> Date d'arrivée</th>
                <td> {{$reservation->date_a}} </td>

            </tr>
            <tr>
                <th>Date de sortie</th>
                <td> {{$reservation->date_s}} </td>

            </tr>
            <tr>
                <th>Nombre de nuits</th>
                <td> {{$reservation->nuits}} </td>

            </tr>
            <tr>
                <th>Etat</th>
                <td> {{$reservation->etat}} </td>

            </tr>
            <tr>
                <th>Prix Total</th>
                <td>{{$reservation->total-$reservation->reduction()}} dhs </td>
            </tr>

        </tbody></table>
    </div>

    @endsection