@extends('layouts.master')
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content')
<div class="col-lg-10 col-lg-offset-1">
<h1>Modification de la réservation</h1>
<hr/>
<table class="table">
  {!! Form::model($visite, [
  'method' => 'PATCH',
  'url' => ['visite', $visite->id],
  'class' => 'form-horizontal'
  ]) !!}

  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input id="nbrJrs" type="hidden" name="nbrJrs" value="">
  <tr class="success">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      * {{ $error }} <br>
      @endforeach
    </div>
    @endif
    <td colspan="2"> clients:
    </td>
  </tr>
  <tr class=""> <td>Pièce d'identité:</td>
    <td class="row">
      <div class="col-xs-7">
        <input type="text" class="form-control" name="delivrance" value="passeport" value="{{$visite->client->delevrance}}" disabled>
      </div>
    </td>
  </tr>
  <tr>
    <td>
     CIN:
   </td>
   <td class="row">
    <div class="col-xs-7">
     <input type="text" class="form-control" value="{{$visite->id}}" name="visiteur_id" disabled>
   </div>
 </td>
</tr>
<tr>
  <td>Nom:</td>
  <td class="row">
    <div class="col-xs-7">
     <input class="form-group form-control" type="text" name="nom" value="{{$visite->nom}}" disabled>
   </div>
 </td>
</tr>
<tr>
  <td>  Prénom:     </td>
<!--              Nombre de personnes                    -->

<!--            FIN nombre de personnes  -->
<tr class="success chbr "> <td colspan="2">Chambre:   </td>  </tr>
 <td>N° de chambre</td>
 <td class="row">
    <div class="col-xs-7">
      <input type="text" class="form-control" value="{{$visite->chambre_id}}" disabled>
    </div>
  </td>
<tr class="success"> <td colspan="2">Services:</td></tr>
<tr>
  <td colspan="2">
    <table class="table table-bordered table-condensed table-hover table-striped">
      <tr>
        <th>Service</th> <th>Nombre de jours</th> <th>Prix/jours</th>
      </tr>
      @foreach($services as $service)
      <tr>
        <td>
          {{$service->libelle}}
        </td>

        <td>
         <select name="nombreJrs{{$service->id}}" class="nbrJrs">
         </select>
       </td>
       <td>{{$service->prix}} dhs</td>
     </tr>
     @endforeach
   </table>
 </tr>
 <tr>
  <td class="row">
    <div class="col-xs-7">
      <input class="form-group form-control" type="text" name="prenom" value="{{$visite->prenom}}" disabled>
    </div>
  </td>
</tr>
  <td>
    <input type="submit" name="valider" class="btn btn-success" value="Modifier">
    <input type="reset" name="reset" class="btn btn-info" value="Reset">
  </td>
</tr>
{!! Form::close() !!}
</table>
@if ($errors->any())
<ul class="alert alert-success">
  @foreach ($errors->all() as $error)
  <li>{{ $error }}</li>
  @endforeach
</ul>
@endif
</div>
@endsection
@section('css')
@parent
<link rel="stylesheet" href="{{URL::asset('js/jquery-ui/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('js/chosen/chosen.min.css')}}">
@endsection

<!-- section du code js -->
@section('js')
@parent
<script src="{{URL::asset('js/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('js/chosen/chosen.jquery.min.js')}}" type="text/javascript"></script>

@endsection