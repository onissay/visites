    @extends('layouts.app')

    @section('content')
    <div class="col-lg-10 col-lg-offset-1">
     <div id="page" class="réservations">

        <h1 class="pull-right">زيارات {{$title}}</h1> <br><a href="{{ url('visite/create') }}" class="btn btn-primary btn-sm">زيارة جديدة</a>
        @if(Session::has('fail'))
        <div class="alert-box alert-danger">
            <h2>{{ Session::get('fail') }}</h2>
        </div>
        @endif
        @if(Session::has('success'))
        <div class="alert-box alert-success">
            <h2>{{ Session::get('success') }}</h2>
        </div>
        @endif
        @if(count($visites))
        <div class="table-responsive">
            <table class="table table-bordered table-reflow table-striped table-hover">
                <thead>
                    <tr>
                        <th class="text-center table-header">Actions</th>
                        <th class="text-center table-header">تاريخ الدخول</th>
                        <th class="text-center table-header">القسم</th>
                        <th class="text-center table-header">ر.ب.و</th>
                        <th class="text-center table-header">اﻹسم الشخصي</th>
                        <th class="text-center table-header">اﻹسم العائلي</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($visites as $visite)
                    <tr  class="text-center">
                    <td width="20%">
                        <!-- 
                           <a href="{{ url('visite/' . $visite->id) }}">
                            <button type="submit" class="btn btn-success btn-xs">Afficher</button>
                        </a>

                        <a href="{{ url('visite/' . $visite->id . '/edit') }}">
                            <button type="submit" class="btn btn-primary btn-xs">Modifier</button>
                        </a>

                        -->

                        {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['visite', $visite->id],
                        'style' => 'display:inline'
                        ]) !!}
                        {!! Form::submit('مسح', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                    <td>{{$visite->created_at}}</td>
                    <td> <a href="#" data-toggle="tooltip" title="{{$visite->division->libelle}}">{{ $visite->division->libelle }}</a></td>
                        <td> {{ $visite->visiteur_id }}</td>
                        <td> {{ $visite->visiteur->prenom }}</td>
                        <td> {{ $visite->visiteur->nom}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{$visites->links()}}
        @else
        <br>
        <br>
        <br>
  <div class="text-center"><h3>لاتوجد أية زيارة</h3></div>
        @endif
    </div>
</div>
</div> <!-- col-lg-10 col-lg-offset-1 -->
@endsection
@section('js')
@parent
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });
</script>
@endsection
