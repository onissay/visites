@extends("layouts.app") 
@section('content')
<div class="row">

    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div id="container" style="width: 600px; width:600px;height: 600px; max-width: 600px; margin: 0 auto"></div>
        <div id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>

    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
        <br>
        <br>
        <br>
        <br>
        <h3 class="lighter purple pull-right">ترتيب الزوار العشر اﻷوائل</h3>
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead class="">
                    <tr>
                        <th class="text-center table-header">Action</th>
                        <th class="text-center table-header">عدد الزيارات</th>
                        <th class="text-center table-header">ر.ب.و</th>
                        <th class="text-center table-header">اﻹسم الكامل</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($visiteurs as $visiteur)
                    <tr class="text-center">
                    <td><a href="{{url('/'.$results[$loop->index]->visiteur_id.'/details')}}" class="btn btn-info">تفاصيل</a></td>
                        <td><span class="{{$badges[$loop->index]}}">{{$results[$loop->index]->compteur}}</span></td>
                        <td>{{$results[$loop->index]->visiteur_id}}</td>
                        <td><a href="{{url('/'.$visiteur->id.'/profile')}}">{{$visiteur->getFullName()}}</a></td>
                    </tr>
                @endforeach           
                </tbody>
            </table>
        </div>
        
       

</div>
<br> @endsection 
@section('js') 
@parent
<script src="{{URL::asset('js/highcharts.js')}}"></script>
<script src="{{URL::asset('js/exporting.js')}}"></script>

<script type="text/javascript">
var chart = Highcharts.chart('container', {
    chart: {
            inverted: true,
            polar: false
        },
    title: {
        text: 'زيارات اﻷقسام'
    },

    subtitle: {
        text: ''
    },

    yAxis:{
        title: {
            enabled: true,
            text: ' <b>عدد الزيارات</b>',
            style: {
                fontWeight: 'normal'
            }
        }
    },
    xAxis: {
        categories: {!! json_encode($categories) !!}
    },
    tooltip: {
    formatter: function() {
        return ' عدد زيارات '  + this.x + '</b> هو <b>' + this.y + '</b>';
    }
},

    series: [{
        type: 'column',
        colorByPoint: true,
        data: {!! json_encode($data) !!},
        showInLegend: false
    }]

});



//----------------------------------------------------Pie chart-----------------------------------
   /* Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
            [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                ]
            };
        });

    // Build the chart
    Highcharts.chart('containers', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Visite des divisions'
        },
        size: {
            width: 100,
            hight: 100
        },
        tooltip: {
            pointFormat: '{series.abbr} ({point.count}): <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                size: '70%',
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.percentage:.1f} %</b>',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    connectorColor: 'silver'
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            data: {!! json_encode($data) !!}
        }]
    });
*/
    //------------------date
    Highcharts.chart('container1', {
    chart: {
        type: 'line'
    },
    title: {
        text: 'Monthly Average Temperature'
    },
    subtitle: {
        text: 'Source: WorldClimate.com'
    },
    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    },
    yAxis: {
        title: {
            text: 'Temperature (°C)'
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: {!! json_encode($points) !!}
});

</script>
@endsection