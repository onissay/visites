@extends('layouts.app')
@section('content')
<br>
<div class="row">
<div class="col col-md-8 col-md-offset-1" style="background:azure">
<h3 class="pull-right" > احصاء الزيارات </h3> 
</div>
<br>
<br>
    <div class="col-md-8 col-md-offset-4">
        <div  class="form-inline col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-3" role="form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
                    <button type="submit"  class="btn btn-success btn-sm">بحث</button>
            </div>
            <div class="form-group">
                <select name="year" id="year_id">
                    <option value=""></option>
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>
                </select>
            </div>   
            <div class="form-group has-success">
                <input id="date_fin" name="dateFin" type="text" class="form-control" placeholder="تاريخ النهاية">
            </div> 
            <div class="form-group">
                <input id="date_debut" name="dateDebut" type="text" class="form-control" placeholder="تاريخ البداية">
            </div> 
      </div>
    </div>   
</div>
<br>
<div class="row">
<div class="col col-md-8 col-md-offset-1">
    
    <table class="table table-bordered table-hover hidden">
        <thead class="head-inverse">
            <tr>
            <th class="text-center table-header">تاريخ الزيارة</th>
            <th class="text-center table-header">القسم</th>
            <th class="text-center table-header">اﻹسم الكامل</th>
            <th class="text-center table-header">ر.ب.و</th>
            </tr>
        </thead>
        <tbody id="body">

        </tbody>
    </table>
    </div>
</div>
@endsection
@section('css')
@parent
<link rel="stylesheet" href="{{URL::asset('js/jquery-ui/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('js/chosen/chosen.min.css')}}">
@endsection
@section('js')
@parent
<script src="{{URL::asset('js/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('js/chosen/chosen.jquery.min.js')}}" type="text/javascript"></script>

<script>
  $(document).ready(function(){
    $("select").chosen({
      disable_search_threshold: 10,
      no_results_text: "ﻻيوجد",
      width: "100%",
      placeholder_text:"جميع اﻷقسام"
    });
    $.datepicker.regional['fr'] = {clearText: 'Effacer', clearStatus: '',
    closeText: 'Fermer', closeStatus: 'Fermer sans modifier',
    prevText: '&lt;Préc', prevStatus: 'Voir le mois précédent',
    nextText: 'Suiv&gt;', nextStatus: 'Voir le mois suivant',
    currentText: 'Courant', currentStatus: 'Voir le mois courant',
    monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
    'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
    monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
    'Jul','Aoû','Sep','Oct','Nov','Déc'],
    monthStatus: 'Voir un autre mois', yearStatus: 'Voir un autre année',
    weekHeader: 'Sm', weekStatus: '',
    dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
    dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
    dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
    dayStatus: 'Utiliser DD comme premier jour de la semaine', dateStatus: 'Choisir le DD, MM d',
    dateFormat: 'dd/mm/yy', firstDay: 0, 
    initStatus: 'Choisir la date', isRTL: false};
    $.datepicker.setDefaults($.datepicker.regional['fr']);
    $( "#date_debut" ).datepicker({
      dateFormat: "d/m/yy",
   
    });
    $( "#date_fin" ).datepicker({
      dateFormat: "d/m/yy",
 });
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    // Récupérer les chambres
    $('button').on('click', function() {
        
      $.ajax({
        method:'POST',
        url: '{{url("/getvisites")}}',
        data:{'division_id':$('#division_id').val(),'dateDebut':$('#date_debut').val(),'dateFin':$('#date_fin').val()},
        beforeSend: function( xhr ) {
          xhr.overrideMimeType( "text/plain; charset=utf-8" );
        }
      })
      .done(function( data ) {
        $('table').removeClass('hidden');
       $('#body').html(data);
      // alert(data);
     });
    });
//id client vérification

});
</script>
@endsection
@section('css')
@parent
<link rel="stylesheet" href="{{URL::asset('js/jquery-ui/jquery-ui.min.css')}}">
@endsection