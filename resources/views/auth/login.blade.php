@extends('layouts.master-login')
@section('content')
<body class="login-layout">
    <div class="main-container">
        <div class="main-content">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="login-container">
                        <div class="center">
                            <h1>
                            <i class="icon-bed green"></i>
                                <span class="red">LabInventory</span>
                            </h1>
                            <h4 class="blue">&copy;  LabSystems</h4>
                        </div>

                        <div class="space-6"></div>

                        <div class="position-relative">
                            <div id="login-box" class="login-box visible widget-box no-border">
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <h4 class="header blue lighter bigger">
                                            <i class="icon-coffee green"></i>
                                            Entrez vos informations
                                        </h4>

                                        <div class="space-6"></div>

                                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                                         {!! csrf_field() !!}
                                         <fieldset>

                                            <label class="block clearfix;form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                <span class="block input-icon input-icon-right">
                                                    <input type="text" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" />
                                                    <i class="icon-user"></i>
                                                </span>
                                                @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                @endif                                                      
                                            </label>

                                            <label class="block clearfix;form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                <span class="block input-icon input-icon-right">
                                                    <input type="password" class="form-control" placeholder="Password" name="password" />
                                                    <i class="icon-lock"></i>
                                                </span>
                                                @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                                @endif
                                            </label>

                                            <div class="space"></div>

                                            <div class="clearfix">
                                                <label class="inline">
                                                    <input type="checkbox" class="ace" />
                                                    <span class="lbl"> Remember Me</span>
                                                </label>

                                                <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                                    <i class="icon-key"></i>
                                                    Login
                                                </button>
                                            </div>

                                            <div class="space-4"></div>
                                        </fieldset>
                                    </form>
                                </div><!-- /widget-main -->
                                <div class="toolbar clearfix">
                                    <div>
                                        <a class="forgot-password-link" href="{{ url('/password/reset') }}">Mot de passe oublié?</a>

                                    </div>
                                    <div>
                                       <a class="user-signup-link" href="{{URL('/register')}}">
                                        <i class="icon-plus-sign"></i>
                                        S'inscrire
                                    </a>
                                </div>

                            </div>
                        </div><!-- /widget-body -->
                    </div><!-- /login-box -->
                </div><!-- /position-relative -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div>
</div><!-- /.main-container -->
@endsection
