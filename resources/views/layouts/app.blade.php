<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
 
    <meta name="description" content="Common UI Features &amp; Elements" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    @section('css')
    <!-- basic styles -->

    <link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{URL::asset('css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{URL::asset('css/font-awesome2.min.css')}}" />

        <!--[if IE 7]>
          <link rel="stylesheet" href="{{URL::asset('css/font-awesome-ie7.min.css')}}" />
          <![endif]-->

          <!-- page specific plugin styles -->

          <link rel="stylesheet" href="{{URL::asset('css/jquery-ui-1.10.3.custom.min.css')}}" />
          <link rel="stylesheet" href="{{URL::asset('css/jquery.gritter.css')}}" />

          <!-- fonts -->

          <link rel="stylesheet" href="{{URL::asset('css/ace-fonts.css')}}" />

          <!-- ace styles -->

          <link rel="stylesheet" href="{{URL::asset('css/ace.min.css')}}" />
          <link rel="stylesheet" href="{{URL::asset('css/ace-rtl.min.css')}}" />
          <link rel="stylesheet" href="{{URL::asset('css/ace-skins.min.css')}}" />

        <!--[if lte IE 8]>
          <link rel="stylesheet" href="{{URL::asset('css/ace-ie.min.css')}}" />
          <![endif]-->

          <!-- inline styles related to this page -->

          <style>
            .spinner-preview {
                width:100px;
                height:100px;
                text-align:center;
                margin-top:60px;
            }

            .dropdown-preview {
                margin:0 5px;
                display:inline-block;
            }
            .dropdown-preview  > .dropdown-menu {
                display: block;
                position: static;
                margin-bottom: 5px;
            }
        </style>
        @show


    </head>

    <body>
        <!-- navigation bar -->
        <div class="navbar navbar-default" id="navbar">
            <script type="text/javascript">
                try{ace.settings.check('navbar' , 'fixed')}catch(e){}
            </script>

            <div class="navbar-container" id="navbar-container">
                <div class="navbar-header pull-left">
                    <a href="/" class="navbar-brand">
                        <small>
                            <img src="{{URL::asset('min-int.jpg')}}" width="80px" alt="">
                            <span>تدبير الزيارات</span> 
                        </small>
                    </a><!-- /.brand -->
                </div><!-- /.navbar-header -->
               

                <div class="navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="orange2">
                            <a  href="{{URL('/')}}">
                                <i class="fa fa-pie-chart icon-animated-hand-pointer"></i> إحصائيات
                            </a>

                        </li>
                        <!-- visite -->
                        <li class="orange">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="fa fa-calendar-check-o icon-animated-hand-pointer"></i> زيارات
                                <i class="icon-caret-down"></i>
                            </a>

                            <ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                             <li>
                                    <a href="{{URL('visite/create')}}" class="pull-right">
                                        اضافة
                                        <span class="icon-plus-sign"></span>
                                    </a>
                                </li>
                              <li>
                                    <a href="{{URL('/visite/find')}}" class="pull-right">
                                        بحث
                                        <span class="icon-search"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{URL('visite/duJour')}}" class="pull-right">
                                        زيارات اليوم
                                        <span class="icon-list"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{URL('visite')}}" class="pull-right">
                                        جميع الزيارات
                                        <span class="icon-list"></span>
                                    </a>
                                </li>

                                <li>
                                    <div class="progress progress-mini ">
                                        <div style="width:100%" class="progress-bar grey"></div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- end visite  -->
                        
                        <li class="grey">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class=" icon-eye-open icon-animated-vertical"></i>اﻷقسام
                                <i class="icon-caret-down"></i>
                            </a>
                            <ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                                <li>
                                    <a href="{{URL('division')}}" class="pull-right">
                                    اللائحة
                                <span class="icon-list"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{URL('division/create')}}" class="pull-right">
                                        إضافة
                                        <span class="icon-plus-sign"></span>
                                    </a>
                                </li>
                                <li>
                                    <div class="progress progress-mini ">
                                        <div style="width:100%" class="progress-bar grey"></div>
                                    </div>
                                </li>
                            </ul>
                        </li>


                        <!-- visiteur -->
                        <li class="purple">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i class=" icon-user icon-animated-vertical"></i>زوار
                            <i class="icon-caret-down"></i>
                        </a>
                        <ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                            <li>
                                <a href="{{URL('visiteur/blackliste')}}" class="pull-right">
                            اللائحة السوداء
                            <span class="icon-list"></span>
                                </a>
                            </li>
                            <li>
                                <a href="{{URL('visiteur/addtoblackliste')}}" class="pull-right">
                                    إضافة إلى اللائحة السوداء
                                    <span class="icon-plus-sign"></span>
                                </a>
                            </li>
                            <li>
                                <div class="progress progress-mini ">
                                    <div style="width:100%" class="progress-bar grey"></div>
                                </div>
                            </li>
                        </ul>
                    </li>
                        <!-- end visiteur -->
                        
                        <li class="light-blue">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <i class="fa fa-user-circle fa-2x"></i>
                                <span class="user-info">
                                    <small>Bonjour,</small>
                                   
                                </span>

                                <i class="icon-caret-down"></i>
                            </a>

                            <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                  <a href="{{ url('/logout') }}" 
                                  onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                                  Logout {{Auth::user()}}
                              </a>
                              <form id="logout-form" 
                              action="{{ url('/logout') }}" 
                              method="POST" 
                              style="display: none;">
                              {{ csrf_field() }}
                          </form>
                      </li>
                  </ul>
              </li>
          </ul><!-- /.ace-nav -->
      </div><!-- /.navbar-header -->
    
  </div><!-- /.container -->
</div>

<div class="main-container" id="main-container">
    @yield('content')
</div>

<!-- <![endif]-->

        <!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='{{URL::asset('js/jquery-1.10.2.min.js')}}'>"+"<"+"/script>");
</script>
<![endif]-->
@section('js')
<!-- ace settings handler -->

<script src="{{URL::asset('js/ace-extra.min.js')}}"></script>

<!-- HTML5 shim and Respondjs')}} IE8 support of HTML5 elements and media queries -->

        <!--[if lt IE 9]>
        <script src="{{URL::asset('js/html5shivjs')}}"></script>
        <script src="{{URL::asset('js/respond.min.js')}}"></script>
        <![endif]-->
        <script src="{{URL::asset('js/jquery-2.0.3.min.js')}}"></script>
        <script type="text/javascript">
            if("ontouchend" in document) document.write("<script src=\"{{URL::asset('js/jquery.mobile.custom.min.js')}}\">"+"<"+"/script>");
        </script>
        <script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
        <script src="{{URL::asset('js/typeahead-bs2.min.js')}}"></script>

        <!-- page specific plugin scripts -->

        <!--[if lte IE 8]>
          <script src="{{URL::asset('js/excanvas.min.js')}}"></script>
          <![endif]-->


          <script src="{{URL::asset('js/jquery-ui-1.10.3.custom.min.js')}}"></script>
          <script src="{{URL::asset('js/jquery.ui.touch-punch.min.js')}}"></script>
          <script src="{{URL::asset('js/bootbox.min.js')}}"></script>
          <script src="{{URL::asset('js/jquery.easy-pie-chart.min.js')}}"></script>
          <script src="{{URL::asset('js/jquery.gritter.min.js')}}"></script>
          <script src="{{URL::asset('js/spin.min.js')}}"></script>

          <!-- ace scripts -->

          <script src="{{URL::asset('js/ace-elements.min.js')}}"></script>
          <script src="{{URL::asset('js/ace.min.js')}}"></script>

          <!-- inline scripts related to this page -->

          <script type="text/javascript">
            jQuery(function($) {
                /**
                $('#myTab a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                  console.log(e.target.getAttribute("href"));
                })
                */


                $('#accordion-style').on('click', function(ev){
                    var target = $('input', ev.target);
                    var which = parseInt(target.val());
                    if(which == 2) $('#accordion').addClass('accordion-style2');
                    else $('#accordion').removeClass('accordion-style2');
                });


                var oldie = /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase());
                $('.easy-pie-chart.percentage').each(function(){
                    $(this).easyPieChart({
                        barColor: $(this).data('color'),
                        trackColor: '#EEEEEE',
                        scaleColor: false,
                        lineCap: 'butt',
                        lineWidth: 8,
                        animate: oldie ? false : 1000,
                        size:75
                    }).css('color', $(this).data('color'));
                });

                $('[data-rel=tooltip]').tooltip();
                $('[data-rel=popover]').popover({html:true});


                $('#gritter-regular').on(ace.click_event, function(){
                    $.gritter.add({
                        title: 'This is a regular notice!',
                        text: 'This will fade out after a certain amount of time. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href="#" class="blue">magnis dis parturient</a> montes, nascetur ridiculus mus.',
                        image: $path_assets+'/avatars/avatar1.png',
                        sticky: false,
                        time: '',
                        class_name: (!$('#gritter-light').get(0).checked ? 'gritter-light' : '')
                    });

                    return false;
                });

                $('#gritter-sticky').on(ace.click_event, function(){
                    var unique_id = $.gritter.add({
                        title: 'This is a sticky notice!',
                        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href="#" class="red">magnis dis parturient</a> montes, nascetur ridiculus mus.',
                        image: $path_assets+'/avatars/avatar.png',
                        sticky: true,
                        time: '',
                        class_name: 'gritter-info' + (!$('#gritter-light').get(0).checked ? ' gritter-light' : '')
                    });

                    return false;
                });


                $('#gritter-without-image').on(ace.click_event, function(){
                    $.gritter.add({
                        // (string | mandatory) the heading of the notification
                        title: 'This is a notice without an image!',
                        // (string | mandatory) the text inside the notification
                        text: 'This will fade out after a certain amount of time. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href="#" class="orange">magnis dis parturient</a> montes, nascetur ridiculus mus.',
                        class_name: 'gritter-success' + (!$('#gritter-light').get(0).checked ? ' gritter-light' : '')
                    });

                    return false;
                });


                $('#gritter-max3').on(ace.click_event, function(){
                    $.gritter.add({
                        title: 'This is a notice with a max of 3 on screen at one time!',
                        text: 'This will fade out after a certain amount of time. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href="#" class="green">magnis dis parturient</a> montes, nascetur ridiculus mus.',
                        image: $path_assets+'/avatars/avatar3.png',
                        sticky: false,
                        before_open: function(){
                            if($('.gritter-item-wrapper').length >= 3)
                            {
                                return false;
                            }
                        },
                        class_name: 'gritter-warning' + (!$('#gritter-light').get(0).checked ? ' gritter-light' : '')
                    });

                    return false;
                });


                $('#gritter-center').on(ace.click_event, function(){
                    $.gritter.add({
                        title: 'This is a centered notification',
                        text: 'Just add a "gritter-center" class_name to your $.gritter.add or globally to $.gritter.options.class_name',
                        class_name: 'gritter-info gritter-center' + (!$('#gritter-light').get(0).checked ? ' gritter-light' : '')
                    });

                    return false;
                });
                
                $('#gritter-error').on(ace.click_event, function(){
                    $.gritter.add({
                        title: 'This is a warning notification',
                        text: 'Just add a "gritter-light" class_name to your $.gritter.add or globally to $.gritter.options.class_name',
                        class_name: 'gritter-error' + (!$('#gritter-light').get(0).checked ? ' gritter-light' : '')
                    });

                    return false;
                });


                $("#gritter-remove").on(ace.click_event, function(){
                    $.gritter.removeAll();
                    return false;
                });


                ///////


                $("#bootbox-regular").on(ace.click_event, function() {
                    bootbox.prompt("What is your name?", function(result) {
                        if (result === null) {
                            //Example.show("Prompt dismissed");
                        } else {
                            //Example.show("Hi <b>"+result+"</b>");
                        }
                    });
                });

                $("#bootbox-confirm").on(ace.click_event, function() {
                    bootbox.confirm("Are you sure?", function(result) {
                        if(result) {
                            //
                        }
                    });
                });

                $("#bootbox-options").on(ace.click_event, function() {
                    bootbox.dialog({
                        message: "<span class='bigger-110'>I am a custom dialog with smaller buttons</span>",
                        buttons:            
                        {
                            "success" :
                            {
                                "label" : "<i class='icon-ok'></i> Success!",
                                "className" : "btn-sm btn-success",
                                "callback": function() {
                                    //Example.show("great success");
                                }
                            },
                            "danger" :
                            {
                                "label" : "Danger!",
                                "className" : "btn-sm btn-danger",
                                "callback": function() {
                                    //Example.show("uh oh, look out!");
                                }
                            }, 
                            "click" :
                            {
                                "label" : "Click ME!",
                                "className" : "btn-sm btn-primary",
                                "callback": function() {
                                    //Example.show("Primary button");
                                }
                            }, 
                            "button" :
                            {
                                "label" : "Just a button...",
                                "className" : "btn-sm"
                            }
                        }
                    });
                });



                $('#spinner-opts small').css({display:'inline-block', width:'60px'})

                var slide_styles = ['', 'green','red','purple','orange', 'dark'];
                var ii = 0;
                $("#spinner-opts input[type=text]").each(function() {
                    var $this = $(this);
                    $this.hide().after('<span />');
                    $this.next().addClass('ui-slider-small').
                    addClass("inline ui-slider-"+slide_styles[ii++ % slide_styles.length]).
                    css({'width':'125px'}).slider({
                        value:parseInt($this.val()),
                        range: "min",
                        animate:true,
                        min: parseInt($this.data('min')),
                        max: parseInt($this.data('max')),
                        step: parseFloat($this.data('step')),
                        slide: function( event, ui ) {
                            $this.attr('value', ui.value);
                            spinner_update();
                        }
                    });
                });





                $.fn.spin = function(opts) {
                    this.each(function() {
                        var $this = $(this),
                        data = $this.data();

                        if (data.spinner) {
                            data.spinner.stop();
                            delete data.spinner;
                        }
                        if (opts !== false) {
                            data.spinner = new Spinner($.extend({color: $this.css('color')}, opts)).spin(this);
                        }
                    });
                    return this;
                };

                function spinner_update() {
                    var opts = {};
                    $('#spinner-opts input[type=text]').each(function() {
                        opts[this.name] = parseFloat(this.value);
                    });
                    $('#spinner-preview').spin(opts);
                }



                $('#id-pills-stacked').removeAttr('checked').on('click', function(){
                    $('.nav-pills').toggleClass('nav-stacked');
                });


            });
        </script>
        @show
    </body>
    </html>
