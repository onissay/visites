<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>@yield('title')</title>

		<meta name="description" content="Common UI Features &amp; Elements" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
 @section('css')
		<!-- basic styles -->

		<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet" />
		<link rel="stylesheet" href="{{URL::asset('css/font-awesome.min.css')}}" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="{{URL::asset('css/font-awesome-ie7.min.css')}}" />
		<![endif]-->

		<!-- page specific plugin styles -->

		<link rel="stylesheet" href="{{URL::asset('css/jquery-ui-1.10.3.custom.min.css')}}" />
		<link rel="stylesheet" href="{{URL::asset('css/jquery.gritter.css')}}" />

		<!-- fonts -->

		<link rel="stylesheet" href="{{URL::asset('css/ace-fonts.css')}}" />

		<!-- ace styles -->

		<link rel="stylesheet" href="{{URL::asset('css/ace.min.css')}}" />
		<link rel="stylesheet" href="{{URL::asset('css/ace-rtl.min.css')}}" />
		<link rel="stylesheet" href="{{URL::asset('css/ace-skins.min.css')}}" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="{{URL::asset('css/ace-ie.min.css')}}" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<style>
			.spinner-preview {
				width:100px;
				height:100px;
				text-align:center;
				margin-top:60px;
			}
			
			.dropdown-preview {
				margin:0 5px;
				display:inline-block;
			}
			.dropdown-preview  > .dropdown-menu {
				display: block;
				position: static;
				margin-bottom: 5px;
			}
		</style>
		 @show


	</head>

	<body>
	

		@yield('content')


		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='{{URL::asset('js/jquery-1.10.2.min.js')}}'>"+"<"+"/script>");
</script>
<![endif]-->
@section('js')
		<!-- ace settings handler -->

		<script src="{{URL::asset('js/ace-extra.min.js')}}"></script>

		<!-- HTML5 shim and Respondjs')}} IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="{{URL::asset('js/html5shivjs')}}"></script>
		<script src="{{URL::asset('js/respond.min.js')}}"></script>
		<![endif]-->
<script src="{{URL::asset('js/jquery-2.0.3.min.js')}}"></script>
		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src=\"{{URL::asset('js/jquery.mobile.custom.min.js')}}\">"+"<"+"/script>");
		</script>
		<script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
		<script src="{{URL::asset('js/typeahead-bs2.min.js')}}"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="{{URL::asset('js/excanvas.min.js')}}"></script>
		<![endif]-->
		

		<script src="{{URL::asset('js/jquery-ui-1.10.3.custom.min.js')}}"></script>
		<script src="{{URL::asset('js/jquery.ui.touch-punch.min.js')}}"></script>
		<script src="{{URL::asset('js/bootbox.min.js')}}"></script>
		<script src="{{URL::asset('js/jquery.easy-pie-chart.min.js')}}"></script>
		<script src="{{URL::asset('js/jquery.gritter.min.js')}}"></script>
		<script src="{{URL::asset('js/spin.min.js')}}"></script>

		<!-- ace scripts -->

		<script src="{{URL::asset('js/ace-elements.min.js')}}"></script>
		<script src="{{URL::asset('js/ace.min.js')}}"></script>

		<!-- inline scripts related to this page -->

		<script type="text/javascript">
			function show_box(id) {
			 jQuery('.widget-box.visible').removeClass('visible');
			 jQuery('#'+id).addClass('visible');
			}
		</script>
		@show
	</body>
</html>
