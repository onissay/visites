@extends('layouts.app') 
@section('content')
<br>
<br>
<br>
<br>
<br>

<div class="col-lg-10 col-lg-offset-1">
<h3 class="lighter purple">Profile du visiteur ayant la CIN {{$visiteur->id}}</h3>
<div class="profile-user-info profile-user-info-striped">
<div class="profile-info-row">
<div class="profile-info-name"> Non Complet </div>

<div class="profile-info-value">
<span  id="username">{{$visiteur->getFullName()}}</span>
</div>
</div>
@if($visiteur->adresse)
<div class="profile-info-row">
<div class="profile-info-name"> Adresse </div>

<div class="profile-info-value">
<i class="fa fa-map-marker light-orange bigger-110"></i>
<span  id="country">{{$visiteur->adresse}}</span>
</div>
</div>
@endif
@if($visiteur->dateNaissance)
<div class="profile-info-row">
<div class="profile-info-name"> Date Naissance </div>

<div class="profile-info-value">
<span  id="age">{{$visiteur->dateNaissance}} </span>
</div>
</div>
@endif
@if($visiteur->tel)
<div class="profile-info-row">
<div class="profile-info-name"> Téléphone </div>

<div class="profile-info-value">
<span  id="signup">{{$visiteur->tel}} </span>
</div>
</div>
@endif
@if(count($visiteur->visites))
<div class="profile-info-row">
<div class="profile-info-name"> Nbre de visites </div>

<div class="profile-info-value">
<span  id="login"><a href="{{url('/'.$visiteur->id.'/details')}}">{{count($visiteur->visites)}}</a></span>
</div>
</div>
@endif

</div>
<div> <!-- <div class="col-lg-10 col-lg-offset-1"> -->
@endsection