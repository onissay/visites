    @extends('layouts.app')

    @section('content')
    <div class="col-lg-10 col-lg-offset-1">
     <div id="page" class="réservations">

        <h1 class="pull-right">اللائحة السوداء</h1> <br><a href="{{ url('visiteur/addtoblackliste') }}" class="btn btn-primary  btn-sm">إضافة منع</a>
        @if(Session::has('fail'))
        <div class="alert-box alert-danger">
            <h2>{{ Session::get('fail') }}</h2>
        </div>
        @endif
        @if(Session::has('success'))
        <div class="alert-box alert-success">
            <h2>{{ Session::get('success') }}</h2>
        </div>
        @endif
        @if(count($visiteurs))
        <div class="table-responsive">
            <table class="table table-bordered table-reflow table-striped table-hover">
                <thead>
                    <tr>
                        <th class="table-header text-center">Actions</th>
                        <th class="table-header text-center">السبب</th>
                        <th class="table-header text-center">اﻹسم العائلي</th>
                        <th class="table-header text-center">اﻹسم الشخصي</th>
                        <th class="table-header text-center">ر.ب.و</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach ($visiteurs as $visiteur)
                    <tr  class="text-center">
                        <td width="20%">
                            <!-- 
                            <a href="{{ url('visite/' . $visiteur->id) }}">
                                <button type="submit" class="btn btn-success btn-xs">Afficher</button>
                            </a>

                            -->
                            <a href="{{ url('visiteur/' . $visiteur->id . '/unblock') }}">
                                <button type="submit" class="btn btn-success btn-xs">سماح</button>
                            </a>

                         </td>
                        <td>{{$visiteur->reason}}</td>
                        <td> {{ $visiteur->nom}}</td>
                        <td> {{ $visiteur->prenom }}</td>
                        <td> {{ $visiteur->id }}</td>


                </tr>
                @endforeach
            </tbody>
        </table>
        {{$visiteurs->links()}}
        @else
        <br>
        <br>
        <br>
        <div class="text-center"><h3>لاتوجد أي عنصر في اللائحة السوداء</h3></div>
        @endif
    </div>
</div>
</div> <!-- col-lg-10 col-lg-offset-1 -->
@endsection
@section('js')
@parent
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });
</script>
@endsection
