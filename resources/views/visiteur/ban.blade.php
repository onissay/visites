@extends('layouts.app')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="col-lg-10 col-lg-offset-1">
  @isset($error)
  <br>
  <div class="alert alert-danger">
  <button type="button" class="close" data-dismiss="alert">
    <i class="icon-remove"></i>
  </button>

  <strong>
    <i class="icon-remove"></i>
    Erreur
  </strong><br>
      {{$error}}
  <br>
</div>
  @endisset
  <br>
<table class="table table-condensed table-responsive">
  @if (count($errors) > 0)
  <div class="alert bg-danger" role="alert">
    @foreach ($errors->all() as $error)
  <svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"/></svg>
    {{ $error }} <br>
    @endforeach
  </div>
  @endif
  {!! Form::open(['url' => 'visiteur/store', 'class' => 'form-group form-control']) !!}
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <tr class="page-header" style="background:aliceblue">
  <td></td>
 <td class=" pull-right"><h1>  منع الزائر</h1>
 </td>
</tr>
<tr>
<td class="row">
      <div class="col-xs-7">
       <input type="text" class="form-control" dir="rtl" name="id" id="id" required>
     </div>
</td>
<td>
 رقم البطاقة الوطنية:
</td>

</tr>
<tr>
  <td class="row">
      <div class="col-xs-7">
        <input dir="rtl" class="form-group form-control" type="text" name="prenom" id="prenom" required>
      </div>
    </td>
<td>  الاسم الشخصي:     </td>
</tr>
<tr>
  <td class="row">
      <div class="col-xs-7">
        <input dir="rtl" class="form-group form-control" type="text" name="nom" id="nom" required>
      </div>
    </td>
<td>الاسم العائلي:</td>
</tr>
<tr>
  <td class="row">
      <div class="col-xs-7">
        <input dir="rtl" class="form-group form-control" type="text" name="adresse" id="adresse" >
      </div>
    </td>
<td>العنوان</td>
</tr>
<tr>
  <td class="row">
      <div class="col-xs-7">
        <input class="form-group form-control" type="text" name="dateNaissance" id="dateNaissance" >
      </div>
    </td>
<td>تاريخ الازدياد</td>
</tr>
<tr>
  <td class="row">
      <div class="col-xs-7">
        <input class="form-group form-control" type="text" name="tel" id="tel" >
      </div>
    </td>
<td>الهاتف</td><br>

</tr>

<tr>
<td>
    <div class="col-xs-7">
    <input class="form-group form-control" dir="rtl" type="text" name="reason" id="reason" >
    </div>
  </td>
  <td>
    <label class="block clearfix">
      {!! Form::label('reason_id','السبب') !!}
    </label>
  </td>

</tr>
 <tr> <td>
  <input type="submit" name="valider" class="btn btn-success" value="تأكبد">
  <input type="reset" name="reset" class="btn btn-info" value="مسح">
</td></tr>
{!! Form::close() !!}
</table>
</div> <!-- col-lg-10 col-lg-offset-1 -->
@endsection
<!-- section du code css -->
@section('css')
@parent
<link rel="stylesheet" href="{{URL::asset('js/jquery-ui/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('js/chosen/chosen.min.css')}}">
@endsection

<!-- section du code js -->
@section('js')
@parent
<script src="{{URL::asset('js/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('js/chosen/chosen.jquery.min.js')}}" type="text/javascript"></script>

<script>
  $(document).ready(function(){
 $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $("select").chosen({
      disable_search_threshold: 10,
      no_results_text: "N'existe pas",
      width: "95%",
      placeholder_text:"القسم - المصلحة"
    });
    $.datepicker.regional['fr'] = {clearText: 'Effacer', clearStatus: '',
    closeText: 'Fermer', closeStatus: 'Fermer sans modifier',
    prevText: '&lt;Préc', prevStatus: 'Voir le mois précédent',
    nextText: 'Suiv&gt;', nextStatus: 'Voir le mois suivant',
    currentText: 'Courant', currentStatus: 'Voir le mois courant',
    monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
    'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
    monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
    'Jul','Aoû','Sep','Oct','Nov','Déc'],
    monthStatus: 'Voir un autre mois', yearStatus: 'Voir un autre année',
    weekHeader: 'Sm', weekStatus: '',
    dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
    dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
    dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
    dayStatus: 'Utiliser DD comme premier jour de la semaine', dateStatus: 'Choisir le DD, MM d',
    dateFormat: 'dd/mm/yy', firstDay: 0, 
    initStatus: 'Choisir la date', isRTL: false};
    $.datepicker.setDefaults($.datepicker.regional['fr']);
    $( "#dateNaissance" ).datepicker({
      dateFormat: "d/m/yy",
   
    });
//id client vérification
$('#id').on('input', function(event) {
 $.ajax({
  method:'POST',
  url: '{{url("/checkVisiteur")}}',
  data:{'id':$(this).val()},
  beforeSend: function( xhr ) {
    xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
  }
})
 .done(function( data ) {
  if(data){
    data = $.parseJSON(data);
    $('input[name="nom"]').attr({
      value: data.nom
    });
    $('input[name="prenom"]').attr({
      value: data.prenom
    });
    $('input[name="dateNaissance"]').attr({
      value: data.date_naissance
      });
    $('input[name="tel"]').attr({
      value: data.tel
    });
    $('input[name="adresse"]').attr({
      value: data.adresse
    });
    $('input[name="reason"]').attr({
      value: data.reason,
    });
  }else{
  $('input[name="nom"]').attr({ value: ''}).removeAttr('disabled');
  $('input[name="prenom"]').attr({value: ''}).removeAttr('disabled');
  $('input[name="adresse"]').attr({value: ''}).removeAttr('disabled');
  $('input[name="dateNaissance"]').attr({value: ''}).removeAttr('disabled');
  $('input[name="tel"]').attr({value: ''}).removeAttr('disabled');
 }
});
});
});
</script>
@endsection