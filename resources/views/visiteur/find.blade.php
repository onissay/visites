@extends('layouts.app')
@section('content')
<br>
<div class="row">
<div class="col col-md-8 col-md-offset-1">
<h3>Cherchez les visiteurs:</h3> 
</div>
    <div class="col-md-8 col-md-offset-4">
        <div  class="form-inline col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-3" role="form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <input id="cin" name="cin" type="text" class="form-control" placeholder="CIN">
            </div>    
            <div class="form-group has-success">
                <input id="nom" name="nom" type="text" class="form-control" placeholder="Nom">
            </div>    
      </div>
    </div>   
</div>
<br>
<div class="row">
<div class="col col-md-8 col-md-offset-1">
    
   
        <div id="body">

        </div>
    </div>
</div>
@endsection
@section('css')
@parent
<link rel="stylesheet" href="{{URL::asset('js/jquery-ui/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('js/chosen/chosen.min.css')}}">
@endsection
@section('js')
@parent
<script src="{{URL::asset('js/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('js/chosen/chosen.jquery.min.js')}}" type="text/javascript"></script>

<script>
  $(document).ready(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    // Récupérer les chambres
    $('input').on('input', function() {
        
      $.ajax({
        method:'POST',
        url: '{{url("/getVisiteur")}}',
        data:{'cin':$('#cin').val(),'nom':$('#nom').val()},
        beforeSend: function( xhr ) {
          xhr.overrideMimeType( "text/plain; charset=utf-8" );
        }
      })
      .done(function( data ) {
       $('#body').html(data);
      // alert(data);Request $request
     });
    });
//id client vérification

});
</script>
@endsection
@section('css')
@parent
<link rel="stylesheet" href="{{URL::asset('js/jquery-ui/jquery-ui.min.css')}}">
@endsection