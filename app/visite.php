<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visite extends Model
{
    public function visiteur()
    {
        return $this->belongsTo('App\Visiteur');
    }
    public function division()
    {
        return $this->belongsTo('App\Division');
    }

}
