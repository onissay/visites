<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Division;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;
class DivisionController extends Controller
{
    
    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        $divisions = Division::paginate(10);
        return view('division.index', compact('divisions'));
        if(Auth::user()){
            
            
        }
        return redirect('login');
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
    public function create()
    {
        return view('division.create');
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(Request $request)
    {
        Division::create($request->all());
        
        Session::flash('flash_message_added', 'Division ajouté avec succée!');
        
        
        return redirect('division');
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    *
    * @return Response
    */
    public function show($id)
    {
        $division = Division::findOrFail($id);
        $point['name'] = $division->libelle;
        for($i=1;$i<=12;$i++){
         $point['data'][$i] = 0;
        }
        foreach($division->visites as $index=>$visite){
           $point['data'][$visite->created_at->format('n')]++; 
        }
         $point['data'] = array_values($point['data']);
         $point[] = $point;
        return view('division.show', ['division'=>$division,'point'=>$point]);
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    *
    * @return Response
    */
    public function edit($id)
    {
        $division = Division::findOrFail($id);
        
        return view('division.edit', compact('division'));
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    *
    * @return Response
    */
    public function update($id, Request $request)
    {
        
        $division = Division::findOrFail($id);
        $division->update($request->all());
        
        Session::flash('flash_message_added', 'Division modifiée!');
        
        return redirect('division');
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    *
    * @return Response
    */
    public function destroy($id)
    {
        Division::destroy($id);
        
        Session::flash('flash_message_deleted', 'Division supprimé avec succée!');
        
        return redirect('division');
    }
    
}
