<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Visite;
use App\Visiteur;
use App\Division;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;
class VisiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
            $visites = Visite::paginate(10);
            return view( 'visite.index', ['visites'=>$visites,'title'=>''] );
    }
    
    public function visiteDujour(){
        $dateDuJour = Carbon::now()->format('Y-m-d');
        $visites = Visite::whereDate('created_at','=',$dateDuJour)->paginate(10);  
        return view('visite.index', ['visites'=>$visites,'title'=>Carbon::now()->format('d/m/Y')]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
      
        $divisions = Division::pluck('libelle')->prepend('');
      
        return view('visite.create', ['divisions'=>$divisions]);

        //return redirect('visite');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if(!$request->input('division_id')){
            $divisions = Division::pluck('abbr')->prepend('');
            return view('visite.create', ['divisions'=>$divisions,'error'=>'Veuillez choisir la division']);
        }
        $visite = new Visite();
        $visite->visiteur_id = strtoupper($request->input('visiteur_id'));
        $visite->division_id = $request->input('division_id');
         $visiteur = Visiteur::find($request->input('visiteur_id'));
        if (!$visiteur){
            $visiteur = new Visiteur();
            $visiteur->id = strtoupper( $request->input('visiteur_id') );
            $visiteur->nom = $request->input('nom');
            $visiteur->prenom =  $request->input('prenom');
            $visiteur->adresse = $request->input('adresse');
            $visiteur->tel = $request->input('tel');
            if( $request->input('date_naissance') )
                $visiteur->dateNaissance =Carbon::createFromFormat('d/m/Y', $request->input('date_naissance'))->format('Y-m-d');
            $visiteur->save();
        }
        $visite->save();
       // Session::flash('flash_message_added', 'Visite N°'.$request->input('id').' est ajoutée avec succée!');
        return redirect('visite/duJour');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $visite = Visite::findOrFail($id);
        return view('visite.show', compact('visite','type','vue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $visite = Visite::findOrFail($id);
        return view('visite.edit', compact('visite','divisions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        $visite = Visite::findOrFail($id);
        $visite->update($request->all());
       
        Session::flash('flash_message_added', 'Visite N°'.$id.' est modifiée!');

        return redirect('visite/duJour');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Visite::destroy($id);

        Session::flash('flash_message_deleted', 'Visite N°: '.$id.' est supprimée avec succée!');

        return back();
    }
    public function find()
    {
        $divisions = Division::pluck('libelle')->prepend('');
        return view('visite.find', ['divisions'=>$divisions]);
    }
    public function check(Request $request)
    {
        $visiteur = Visiteur::find( strtoupper($request->input('id')) );
        if($visiteur){
          $dateNaissance = $visiteur->dateNaissance==null?'':Carbon::createFromFormat('Y-m-d',$visiteur->dateNaissance)->format('d/m/Y') ;
        $error = '';
        if($visiteur->isBlocked)
          $error = '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert">
              <i class="ace-icon fa fa-times"></i>
          </button>

          <strong>
              <i class="ace-icon fa fa-times"></i>
             Visiteur bloqué
          </strong>'.
          $visiteur->reason
          .'<br>
        </div>';
          return ['nom'=>$visiteur->nom,'prenom'=>$visiteur->prenom,'adresse'=>$visiteur->adresse,'tel'=>$visiteur->tel,'date_naissance'=>$dateNaissance,'isBlocked'=>$visiteur->isBlocked,'error'=>$error];
        }else
          return NULL;
    }
    public function getVisites(Request $request)
    {
        // Convert with Carbon to the proper date format
        $response = "";
        if($request->input('dateDebut') && $request->input('dateFin')){
             $dateDebut = Carbon::createFromFormat('d/m/Y H:i:s', $request->input('dateDebut').' 00:00:00')->toDateTimeString();
             $dateFin = Carbon::createFromFormat('d/m/Y H:i:s', $request->input('dateFin').' 23:59:59')->toDateTimeString();
             $visites = Visite::whereBetween('created_at',[$dateDebut,$dateFin])->get();             
        }elseif( $request->input('dateDebut') ){
            $dateDebut = Carbon::createFromFormat('d/m/Y H:i:s', $request->input('dateDebut').' 00:00:00')->toDateTimeString();
            $visites = Visite::where('created_at','>=',$dateDebut)->get();  
        }elseif( $request->input('dateFin') ){
            $dateFin = Carbon::createFromFormat('d/m/Y H:i:s', $request->input('dateFin').' 23:59:59')->toDateTimeString();
            $visites = Visite::whereDate('created_at','<',$dateFin)->get();    
        }else{
                 $visites = Visite::all();
        }
        //construction of the HTML response
        $html = '';        
        foreach ($visites as $visite) {
            if( $request->input('division_id') && $request->input('division_id')!=$visite->division_id )
                continue;
            $html .= 
            '<tr class="text-center">
                <td>'.$visite->division->libelle.'</td>
                <td>'.$visite->created_at.'</td>
                <td>'.$visite->visiteur->getFullName().'</td>
                <td><b>'.$visite->visiteur_id.'</b></td>
            </tr>';
    }
    return $html;
    }
}
