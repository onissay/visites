<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Division;
use App\Visite;
use App\Visiteur;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = DB::table('visites')
        ->select(DB::raw('count(*) as compteur , visiteur_id'))
        ->groupBy('visiteur_id')
        ->orderBy('compteur','desc')
        ->take(10)
        ->get();
        $visiteurs = [];
        foreach($results as $result)
          $visiteurs[] = Visiteur::find($result->visiteur_id);
        
       $divisions = Division::all();
        $data = [];
        $total = Visite::all()->count();
        if($total == 0){
            return redirect()->action('VisiteController@create');
        }

        $points = [];
        $categories = [];
        foreach($divisions as $division){
            /*  $point['abbr'] = $division->abbr;
            $point['name'] = $division->libelle;
            $point['count'] = $division->visites->count();
            $point['y'] = $point['count']*100/$total;*/
            $point = [];

            $point['name'] = $division->libelle;
            for($i=1;$i<=12;$i++){
                $point['data'][$i] = 0;
            }
            foreach($division->visites as $index=>$visite){
                $point['data'][$visite->created_at->format('n')]++; 
            }
            $point['data'] = array_values($point['data']);
            $points[] = $point;
            $data[] = $division->visites->count();
            $categories[] = $division->libelle;
        }
        $badges = [
                'badge badge-pink',
               'badge badge-yellow',
               'badge badge-inverse',
               'badge badge-purple',
               'badge badge-info',
               'badge badge-danger',
               'badge badge-warning',
               'badge badge-success',
               'badge badge-grey',
               'badge'
            ];
        return view('reporting.statistics',['data'=>$data,'categories'=>$categories,'total'=>$total,'visiteurs'=>$visiteurs,'results'=>$results,'badges'=>$badges,'points'=>$points]);
        
        echo '<pre>';
        print_r($visiteurs);
        echo '</pre>';
    }

    public function details(Visiteur $visiteur)
    {
      //  $visites = Visite::where('visiteur_id',$visiteur->id)->get();
      $visites = $visiteur->visites;
        return view('details',['visiteur'=>$visiteur,'visites'=>$visites]);
    }
}
