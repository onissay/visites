<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Visiteur;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;
class VisiteurController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $visiteurs = Visiteur::paginate(15);
        return view('visiteur.index', compact('visiteurs'));
        if(Auth::user()){
        

    }
    return redirect('login');
}
public function blackliste()
{
    $visiteurs = Visiteur::where('isBlocked','=',1)->paginate(10);
    return view('visiteur.blackliste', compact('visiteurs'));
}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('visiteur.ban');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $visiteur = Visiteur::firstOrNew(['id'=>$request->input('id')]);

        $visiteur->nom = $request->input('nom');
        $visiteur->prenom =  $request->input('prenom');
        $visiteur->adresse = $request->input('adresse');
        $visiteur->tel = $request->input('tel');
        $visiteur->isBlocked =  true;
        $visiteur->reason = $request->input('reason');
        if( $request->input('dateNaissance') )
            $visiteur->dateNaissance =Carbon::createFromFormat('d/m/Y', $request->input('dateNaissance'))->format('Y-m-d');
        $visiteur->save();
       // Visiteur::create($request->all());
        Session::flash('flash_message_added', 'Visiteur ajouté avec succée!');
        return redirect('/visiteur/blackliste');
    }
public function unblock(Visiteur $visiteur)
{
    $visiteur->isBlocked = false;
    $visiteur->update();
    return redirect('/visiteur/blackliste');
}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $visiteur = Visiteur::findOrFail($id);

        return view('visiteur.show', compact('visiteur'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $visiteur = Visiteur::findOrFail($id);

        return view('visiteur.edit', compact('visiteur'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        $visiteur = Visiteur::findOrFail($id);
        $visiteur->update($request->all());

        Session::flash('flash_message_added', 'Visiteur modifiée!');

        return redirect('visiteur');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Visiteur::destroy($id);

        Session::flash('flash_message_deleted', 'Visiteur supprimé avec succée!');

        return redirect('visiteur');
    }

    public function profile(Visiteur $visiteur)
    {
        return view('visiteur.profile',compact('visiteur'));
    }
    public function find()
    {
        
        return view('visiteur.find');
    }


    public function getVisiteur(Request $request)
    {
        $visiteurs = null;
        if($request->input('nom')){
            $visiteurs = Visiteur::where( 'nom','like','%'.$request->input('nom').'%' )
                                    ->orWhere( 'prenom','like','%'.$request->input('nom').'%' )
                                    ->get();
        }
        if($request->input('cin')){
        $visiteurs[] = Visiteur::find( strtoupper($request->input('cin')) );
        }
        $response = '';
        if($visiteurs){
        $response = '<h3>Nombre de résultats: '.count($visiteurs).'</h3>';
            foreach($visiteurs as $visiteur){
            $response .= '<div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
            <div class="profile-info-name"> Nom Complet </div>
            
            <div class="profile-info-value">
            <span  id="username">'.$visiteur->getFullName().'</span>
            </div>
            </div>
            <div class="profile-info-row">
            <div class="profile-info-name"> CIN </div>
            
            <div class="profile-info-value">
            <span  id="username">'.$visiteur->id.'</span>
            </div>
            </div>';
            if($visiteur->adresse)
            $response.='<div class="profile-info-row">
            <div class="profile-info-name"> Adresse </div>
            
            <div class="profile-info-value">
            <i class="fa fa-map-marker light-orange bigger-110"></i>
            <span  id="country">'.$visiteur->adresse.'</span>
            </div>
            </div>';
            
            if($visiteur->dateNaissance)
            $response.='<div class="profile-info-row">
            <div class="profile-info-name"> Date Naissance </div>
            
            <div class="profile-info-value">
            <span  id="age">'.$visiteur->dateNaissance.' </span>
            </div>
            </div>';
        
            if($visiteur->tel)
            $response.='<div class="profile-info-row">
            <div class="profile-info-name"> Téléphone </div>
            
            <div class="profile-info-value">
            <span  id="signup">'.$visiteur->tel.' </span>
            </div>
            </div>';
            if(count($visiteur->visites))
            $response.='<div class="profile-info-row">
            <div class="profile-info-name"> Nbre de visites </div>
            
            <div class="profile-info-value">
            <span  id="login"><a href="/'.$visiteur->id.'/details">'.count($visiteur->visites).'</a></span>
            </div>
            </div>
            ';
            $response .='</div><br>';
            }
            return $response;
        }
        return '';
          

    }

}
