<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $fillable = array( 'libelle','abbr','isControlable');
    // protected $prix;

    public function visites()
    {
        return $this->hasMany('App\Visite');
    }
}
