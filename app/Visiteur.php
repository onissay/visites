<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visiteur extends Model
{
    public $incrementing = false;
    protected $fillable = array('id', 'nom','prenom','created_at','updated_at');
    public function getFullName(){
         return $this->prenom.' '.$this->nom;
    }
    public function visites()
    {
        return $this->hasMany('App\Visite');
    }
}
