<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::middleware(['auth'])->group(function () {
    Route::get('/visiteur/blackliste', 'VisiteurController@blackliste');
});
Route::get('/', 'HomeController@index');
Route::get('/{visiteur}/details', 'HomeController@details');
Route::post('/getvisites', 'VisiteController@getVisites');
Route::post('/getVisiteur', 'VisiteurController@getVisiteur');
Route::get('/visiteur/find', 'VisiteurController@find');
Route::get('/visite/find', 'VisiteController@find');
Route::get('/visite/duJour', 'VisiteController@visiteDujour');
Route::get('/{visiteur}/profile', 'VisiteurController@profile');
Route::get('/visiteur/addtoblackliste', 'VisiteurController@create');
Route::post('/visiteur/store', 'VisiteurController@store');   
Route::get('/visiteur/{visiteur}/unblock ', 'VisiteurController@unblock');
Route::resource('division', 'DivisionController');
Route::any('checkVisiteur', 'VisiteController@check');
Route::resource('visite', 'VisiteController');
Route::resource('visiteur', 'VisiteurController');